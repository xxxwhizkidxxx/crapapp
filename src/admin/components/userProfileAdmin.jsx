import React from "react";
import PropTypes from "prop-types";
import UserProfile from "../../shared/components/userProfile";


const UserProfileAdmin = (props) => {
    return (
        <form>
            <div className="row userProfile">
            <table className="col s12">
                <tbody className="col s12">
                    <tr className="col s12">
                        <td className="col col-s3 checkbox">
                        {props.status === 'Active' || props.status === 'Not Confirmed'?
                            <a className="btn btn-large waves-effect waves-light logout" onClick={props.blockUser} id="block">BLOCK USER</a>
                            : <a className="btn btn-large waves-effect waves-light secondary" onClick={props.unblockUser} id="unblock">UNBLOCK USER</a>
                    }
                        </td>
                        <td className="col s5">
                            <UserProfile 
                                username={props.username}
                                highscore={props.highscore}
                            />
                        </td>
                        <td className="col col-s4 status">
                            <label id="userstate">{props.status}</label>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        </form>
    );
}

UserProfileAdmin.propTypes = {
    username: PropTypes.string,
    highscore: PropTypes.number,
    status: PropTypes.string,
    blockUser: PropTypes.func,
    unblockUser: PropTypes.func
}

export default UserProfileAdmin;