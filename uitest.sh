#!/bin/bash

curl -s http://${USER_ENV}-test.cdws.gluo.io/ | grep "<title>Crap App</title>"
RESULT=$?

if [ $RESULT -ne 0 ]
then
        echo ""
        echo "==================================="
        echo "|           UI TESTING            |"
        echo "-----------------------------------"
        echo "FAIL - index.html - Title incorrect!"
        echo ""
        echo "Test Suites: 0 passed, 1 total"
        echo "Tests:       0 passed, 1 total"
        echo "==================================="
        echo ""
        exit 1
else
        echo ""
        echo "==================================="
        echo "|           UI TESTING            |"
        echo "-----------------------------------"
        echo "PASS - index.hmtl - Title correct!"
        echo ""
        echo "Test Suites: 1 passed, 1 total"
        echo "Tests:       1 passed, 1 total"
        echo "==================================="
        echo ""
fi